from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Boolean, Column, ForeignKey, Integer, String, Float

# Define the declarative base
Base = declarative_base()

# Define the model


class User(Base):
    __tablename__ = 'user'

    id = Column(Integer, primary_key=True)
    name = Column(String)
    username = Column(String)
    email = Column(String)
    address = Column(String)
    geo_lat = Column(Float)
    geo_lng = Column(Float)
    phone = Column(String)
    website = Column(String)
    company = Column(String)

    def __init__(self, json_obj):
        separator = ', '
        self.id = json_obj['id']
        self.name = json_obj['name']
        self.username = json_obj['username']
        self.email = json_obj['email']
        address = json_obj['address'].copy()
        geo = address.pop('geo')
        self.address = separator.join(address.values())
        self.geo_lat = float(geo['lat'])
        self.geo_lng = float(geo['lng'])
        self.phone = json_obj['phone']
        self.website = json_obj['website']
        company = json_obj['company']
        self.company = separator.join(company.values())

# The following tables are created but UNUSED


class Post(Base):
    __tablename__ = 'post'
    id = Column(Integer, primary_key=True)
    user_id = Column(Integer, ForeignKey('user.id'))
    title = Column(String)
    body = Column(String)


class Comment(Base):
    __tablename__ = 'comment'
    id = Column(Integer, primary_key=True)
    post_id = Column(Integer, ForeignKey('post.id'))
    name = Column(String)
    email = Column(String)
    body = Column(String)


class Todo(Base):
    __tablename__ = 'todo'
    id = Column(Integer, primary_key=True)
    user_id = Column(Integer, ForeignKey('user.id'))
    title = Column(String)
    completed = Column(Boolean)


class Album(Base):
    __tablename__ = 'album'
    id = Column(Integer, primary_key=True)
    user_id = Column(Integer, ForeignKey('user.id'))
    title = Column(String)


class Photo(Base):
    __tablename__ = 'photo'
    id = Column(Integer, primary_key=True)
    album_id = Column(Integer, ForeignKey('album.id'))
    title = Column(String)
    url = Column(String)
    thumbnail_url = Column(String)
