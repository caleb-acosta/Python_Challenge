import unittest
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from tables import Base, User


class TestUserModel(unittest.TestCase):
    def setUp(self):
        engine = create_engine('sqlite:///:memory:', echo=True)
        Session = sessionmaker(bind=engine)
        self.session = Session()
        Base.metadata.create_all(engine)

    def test_user_creation(self):
        user_data = {
            "id": 1,
            "name": "Leanne Graham",
            "username": "Bret",
            "email": "Sincere@april.biz",
            "address": {
                "street": "Kulas Light",
                "suite": "Apt. 556",
                "city": "Gwenborough",
                "zipcode": "92998-3874",
                "geo": {
                    "lat": "-37.3159",
                    "lng": "81.1496"
                }
            },
            "phone": "1-770-736-8031 x56442",
            "website": "hildegard.org",
            "company": {
                "name": "Romaguera-Crona",
                "catchPhrase": "Multi-layered client-server neural-net",
                "bs": "harness real-time e-markets"
            }
        }

        user = User(user_data)
        self.assertEqual(user.name, "Leanne Graham")
        self.assertEqual(user.username, "Bret")
        self.assertEqual(user.email, "Sincere@april.biz")
        self.assertEqual(
            user.address,
            "Kulas Light, Apt. 556, Gwenborough, 92998-3874")
        self.assertEqual(user.geo_lat, -37.3159)
        self.assertEqual(user.geo_lng, 81.1496)
        self.assertEqual(user.phone, "1-770-736-8031 x56442")
        self.assertEqual(user.website, "hildegard.org")
        self.assertEqual(
            user.company,
            "Romaguera-Crona, Multi-layered client-server neural-net, harness real-time e-markets")


if __name__ == '__main__':
    unittest.main()
