import requests
import json
import configparser
import logging

from sqlalchemy import create_engine, exc
from sqlalchemy.orm import sessionmaker
from sqlalchemy_utils import database_exists, create_database

from tables import User, Base

config = configparser.ConfigParser()
config.read('config.ini')

# Get the database configuration settings
db_host = config['database']['host']
db_port = config['database']['port']
db_username = config['database']['username']
db_password = config['database']['password']
db_name = config['database']['database']

# Get data source list
data_url = config['data_source']

# Create database connection string
database_url = f'postgresql://{db_username}:{db_password}@{db_host}:{db_port}/{db_name}'

engine = create_engine(database_url, echo=True)

# Create database if it does not exist.
if not database_exists(engine.url):
    create_database(engine.url)
else:
    # Connect the database if exists.
    engine.connect()

# Create tables if not exist
Base.metadata.create_all(engine)

# Create a session with the database
Session = sessionmaker(bind=engine)
session = Session()


def get_resource(url):
    try:
        # Get plain JSON Object
        response = requests.get(url)
    except requests.exceptions.ConnectionError:
        logging.error('Failed to establish a new connection')
        return None
    else:
        if response.status_code == 200:
            # Parse JSON
            return json.loads(response.content)
        else:
            logging.error(f'Could not get resource, {response.status_code}')
            return None


def populate_users_table(users):
    for u in users:
        # Append user to current transaction
        session.add(User(u))
    try:
        # Apply current transaction
        session.commit()
    except exc.SQLAlchemyError as e:
        # Undo changes if the transaction raised an error
        session.rollback()
        logging.error(e)


if __name__ == '__main__':
    # Get users list from remote
    users = get_resource(data_url['users'])

    if users is not None:
        populate_users_table(users)
