# Python Challenge

This Python script is the proposed solution to [Bunsan Python Challenge](https://docs.google.com/document/d/1dYX4QWIbHJx_C2HkPDU6wYyM5ozevF8p-HU-HNtDu6U). This script populates a PostgreSQL database with user data from a remote web service, specified in a configuration file.

This script uses a configuration file, extracts the PostgreSQL database settings, then creates a SQLAlchemy engine with this connection string, and creates the database tables specified in the tables.py module.

The script then uses the requests library to retrieve a list of users from the remote data source ([jsonplaceholder.typicode.com](https://jsonplaceholder.typicode.com/)) specified in the configuration file. If the retrieval is successful the script parses the JSON response into a list of user objects and uses SQLAlchemy to insert them into the users table in the PostgreSQL database.

If there is an error during the transaction, the script rolls back any changes made to the database and logs the error. If the retrieval of the remote data source fails, the script logs an error message indicating the connection error or HTTP error code.

### Configuration

Modify the values in the `config.ini` file as needed:

Example

```
[database]
host = my_host
...
```

### Installation 

```console
$ python -m venv env
$ source env/bin/activate (for Unix based systems)
$ .\env\Scripts\activate (for Windows)
$ pip install -r requirements.txt
```

### Usage

Once virtual env is set, additional dependencies were instaled and `config.ini` is correcly set just execute it as follows.

```console
$ python main.py
```

### ER Diagram

![ER Diagram](./er_diagram.png)


### Results of execution

Logs of execution are stored in [out.txt](./out.txt)
